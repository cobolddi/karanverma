<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/FullImageWithTopHeading.php') ?>

<?php @include('template-parts/LeftImageRightContent.php') ?>

<?php @include('template-parts/ExploreTopic.php') ?>


<section class="SocialMediaLinks PaddingBottom">
	<div class="container">
		<div class="SocialMediaBlock Section">
			<h2>You may have noticed, I like saying things in between. I do the same on social media!</h2>
			<p>FOLLOW HIM ELSEWHERE</p>
			<ul>
				<li>
					<a href="#">
						<span class="SocialIcon"><img src="assets/img/youtube.png"></span>
						<span class="SocialText">Subscribe</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span class="SocialIcon"><img src="assets/img/instacolor.png"></span>
						<span class="SocialText">Follow</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span class="SocialIcon"><img src="assets/img/facebook.png"></span>
						<span class="SocialText">Like</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</section>

<section class="RightImageLeftContentWithBg">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6 MobileOnly">
				<div class="RightImageBlock">
					<img src="assets/img/jackmaster.png">
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="LeftContentBlock">
					<h2>DHRUV</h2>
					<h6>THE LOVE STORY OF AN ALCHEMIST</h6>
					<p>Can love inspire you to rise above all challenges and realize your destiny? Some love stories are cherished, some become folklore but the saga of Dhruv & Emma marks the ascension of a man who goes on to become a legend.</p>
					<span class="LinkWithBorder"><a href="#">explore work …till now</a></span>
				</div>
			</div>
			<div class="col-12 col-md-6 DesktopOnly">
				<div class="RightImageBlock">
					<img src="assets/img/jackmaster.png">
				</div>
			</div>
		</div>
	</div>
</section>

<?php @include('template-parts/footer.php') ?>