<section class="RightImageLeftContentWithBg">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6 MobileOnly">
				<div class="RightImageBlock">
					<img src="assets/img/jackmaster.png">
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="LeftContentBlock">
					<h2>JACK & MASTER</h2>
					<h6>A TALE OF FRIENDSHIP, PASSION & GLORY</h6>
					<p>The legend of Jack and Master began twenty years ago in Grinnel’s University, Goa. One is the charismatic ‘Jack of all Trades’ who can play the guitar, sing his way into your heart, be the cyber-wiz next door and also charm the ladies.</p>
					<span class="LinkWithBorder"><a href="#">explore work …till now</a></span>
				</div>
			</div>
			<div class="col-12 col-md-6 DesktopOnly">
				<div class="RightImageBlock">
					<img src="assets/img/jackmaster.png">
				</div>
			</div>
		</div>
	</div>
</section>