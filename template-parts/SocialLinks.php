<section class="SocialMediaLinks">
	<div class="container">
		<div class="SocialMediaBlock Section">
			<h2>You may have noticed, I like saying things in between. I do the same on social media!</h2>
			<p>FOLLOW HIM ELSEWHERE</p>
			<ul>
				<li>
					<a href="#">
						<span class="SocialIcon"><img src="assets/img/youtube.png"></span>
						<span class="SocialText">Subscribe</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span class="SocialIcon"><img src="assets/img/instacolor.png"></span>
						<span class="SocialText">Follow</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span class="SocialIcon"><img src="assets/img/facebook.png"></span>
						<span class="SocialText">Like</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</section>