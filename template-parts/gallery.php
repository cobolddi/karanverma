<section class="Section gallerySection">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="grid-container">
          <div class="grid-item">
            <img src="assets/img/another.png">
          </div>
          <div class="grid-item grid-odd-item">
            <img src="https://source.unsplash.com/1600x900/?water">
          </div>
          <div class="grid-item grid-even-item">
            <img src="https://source.unsplash.com/1600x900/?nature">
          </div>
          <div class="grid-item">
            <img src="https://source.unsplash.com/1600x900/?trending">
          </div>
          <div class="grid-item">
            <img src="https://source.unsplash.com/1600x900/?latest">
          </div>
          <div class="grid-item">
            <img src="https://source.unsplash.com/1600x900/?technology">
          </div>
          <div class="grid-item">
            <img src="https://source.unsplash.com/1600x900/?nature,water">
          </div>
          <div class="grid-item">
            <img src="https://source.unsplash.com/1600x900/?nature,water">
          </div>
          <div class="grid-item">
            <img src="assets/img/smallerteam.png">
          </div>
          <div class="grid-item">
            <img src="assets/img/youtubethumb.png">
          </div>
          <div class="grid-item">
            <img src="assets/img/TopLeftImg.png">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>