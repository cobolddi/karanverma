<section class="Section ExploreTopicBlock">
	<div class="container">
		<h2>I like the power of words, you can see them<br> going inside the brain through their eyes</h2>
		<h6>EXPLORE TOPICS</h6>
		<div class="row">
			<div class="col-6 col-md-3">
				<div class="CardWithHoverText">
					<img src="assets/img/sales.png">
					<h6>SALES & CLOSING</h6>
				</div>
			</div>
			<div class="col-6 col-md-3">
				<div class="CardWithHoverText">
					<img src="assets/img/sales.png">
					<h6>FINANCIAL MANAGEMENT</h6>
				</div>
			</div>
			<div class="col-6 col-md-3">
				<div class="CardWithHoverText">
					<img src="assets/img/sales.png">
					<h6>NEGOTIATIONS</h6>
				</div>
			</div>
			<div class="col-6 col-md-3">
				<div class="CardWithHoverText">
					<img src="assets/img/sales.png">
					<h6>TEAM MANAGEMENT</h6>
				</div>
			</div>
			<div class="col-6 col-md-3">
				<div class="CardWithHoverText">
					<img src="assets/img/sales.png">
					<h6>LEADERSHIP TRAINING</h6>
				</div>
			</div>
			<div class="col-6 col-md-3">
				<div class="CardWithHoverText">
					<img src="assets/img/sales.png">
					<h6>NEW MANAGERS TRAINING</h6>
				</div>
			</div>
			<div class="col-6 col-md-3">
				<div class="CardWithHoverText">
					<img src="assets/img/sales.png">
					<h6>TIME MANAGEMENT</h6>
				</div>
			</div>
			<div class="col-6 col-md-3">
				<div class="CardWithHoverText">
					<img src="assets/img/sales.png">
					<h6>INTERPERSONAL TEAMS</h6>
				</div>
			</div>
		</div>
	</div>
</section>