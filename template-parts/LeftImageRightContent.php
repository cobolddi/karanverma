<section class="Section LeftImageRightContent">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6">
				<img src="assets/img/smallerteam.png">
			</div>
			<div class="col-12 col-md-6">
				<div class="RightContentBlock">
					<h5>SMALLER TEAMS, BIG RESULTS</h5>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tincidunt enim et mi imperdiet molestie. Vivamus luctus molestie porttitor. Integer sit amet dui felis. Curabitur nec mauris tincidunt, malesuada nisl non, tempor enim. Praesent iaculis sodales enim, vitae placerat nunc rhoncus vitae. Nam egestas eu tellus eget accumsan. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla aliquet libero est, et mollis quam fermentum eget. Etiam nec sem quis nunc convallis iaculis a ac mi. Nunc sagittis consectetur mauris, id finibus est sollicitudin pharetra.</p>
				</div>
			</div>
		</div>
	</div>
</section>