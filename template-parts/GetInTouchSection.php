<section class="Section ContactUsSection">
	<div class="container MediumContainer">
		<div class="row">
			<div class="col-12 col-md-7 MobileOnly PBottom3">
				<div class="LeftContentBlock">
					<h2>Send Karan a message</h2>
					<form>
						<div class="row">
							<div class="col-12 col-md-6">
								<label>First Name</label>
								<input type="text" name="">
							</div>
							<div class="col-12 col-md-6">
								<label>Last Name</label>
								<input type="text" name="">
							</div>
							<div class="col-12 col-md-6">
								<label>Contact Number</label>
								<input type="tel" name="">
							</div>
							<div class="col-12 col-md-6">
								<label>Email Address</label>
								<input type="email" name="">
							</div>
							<div class="col-12 col-md-12">
								<label>Message</label>
								<textarea></textarea>
							</div>
							<div class="col-12 col-md-5">
								<input type="button" value="SEND MESSAGE" name="">
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-12 col-md-5">
				<div class="LeftContentBlock">
					<h2>Reach out to the team</h2>
					<h6>SPEAKING ENGAGEMENTS</h6>
					<p>Charu Sharma,<br> charu@karanverma.co.in<br> +91 99443 22112</p>
					<h6>BOOK ENQUIRIES</h6>
					<p>Kiran Reddy,<br> kiran@karanverma.co.in<br> +91 99443 22113</p>
					<h6>MARRIAGE PROPOSALS</h6>
					<p>Divya Verma,<br> divya@karanverma.co.in<br> +91 99443 22114</p>
				</div>
			</div>
			<div class="col-12 col-md-7 DesktopOnly">
				<div class="LeftContentBlock">
					<h2>Send Karan a message</h2>
					<form>
						<div class="row">
							<div class="col-12 col-md-6">
								<label>First Name</label>
								<input type="text" name="">
							</div>
							<div class="col-12 col-md-6">
								<label>Last Name</label>
								<input type="text" name="">
							</div>
							<div class="col-12 col-md-6">
								<label>Contact Number</label>
								<input type="tel" name="">
							</div>
							<div class="col-12 col-md-6">
								<label>Email Address</label>
								<input type="email" name="">
							</div>
							<div class="col-12 col-md-12">
								<label>Message</label>
								<textarea></textarea>
							</div>
							<div class="col-12 col-md-5">
								<input type="button" value="SEND MESSAGE" name="">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>