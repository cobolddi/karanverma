
	</main>
	<footer class="Section">
		<div class="container">
			<div class="FooterContent">
				<p>FOR BOOKINGS & SPEAKING ENGAGEMENTS <a href="mailto:heythere@karanverma.co.in">heythere@karanverma.co.in</a></p>
				<p>Copyright © <script>document.write(new Date().getFullYear())</script>, Karan Verma</p>
			</div>
		</div>
	</footer>

<script src="assets/js/vendor.min.js"></script>
<script src="assets/js/scripts.min.js"></script>
</body>
</html>