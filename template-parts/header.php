<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Karan Verma</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Automation of task with minification of css and js">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/favicon.png">	
    <link href="assets/css/vendor.min.css" rel="stylesheet">
    <link href="assets/css/styles.min.css" rel="stylesheet">
</head>
<body>



<header>

	<!-- TopHeader Portion -->
	<div class="TopHeader">
		<div class="container">
			<div class="LeftRightLinks">
				<p>DHRUV, LOVE STORY OF AN ALCHEMIST. <a href="#" target="_blank">OUT NOW!</a></p>
				<a href="#" target="_blank">BUY ON AMAZON ></a>
			</div>
		</div>
	</div>

	<!-- Responsive Menu -->
	<div class="MobileMenuContainer MobileOnly">		
		<div class="MobileMenu">
			<div class="container">
				<div class="InsideMobileMenu">
					<div class="LogoBox"><a href="#"><img src="assets/img/logo.png"></a></div>
					<button class="menu-button MobileOnly" id="open-button"></button>
				</div>
			</div>
		</div>	
		<div class="content-wrap">
			<div class="menu-wrap">
				<nav class="menu">
					<div class="icon-list">
						<a href="/karanverma"><img src="assets/img/logo.png"></a>
						<ul>
							<li><a href="profile.php">Profile</a></li>
							<li><a href="">Books</a></li>
							<li><a href="">Speaking Engagements</a></li>
							<li><a href="">Press & Media</a></li>
							<li><a href="">Blogs</a></li>
							<li><a href="">Get in Touch</a></li>
						</ul>
						<div class="MobileContactDetails">
							<a href="mailto:heythere@karanverma.co.in">heythere@karanverma.co.in</a>
							<a href="tel:+91 011 4606 4192">+91 011 4606 4192</a>
						</div>
						<ul class="SocialIcons">                                                    
		                    <li>
		                        <a href="#" target="_blank">
		                            <svg>
		                                <use xlink:href="assets/img/cobold-sprite.svg#youtube-icon"></use>
		                            </svg>
		                        </a>
		                    </li> 
		                    <li>
		                        <a href="#" target="_blank">
		                            <svg>
		                                <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
		                            </svg>
		                        </a>
		                    </li>                           
		                    <li>
		                        <a href="#" target="_blank">
		                            <svg>
		                                <use xlink:href="assets/img/cobold-sprite.svg#facebook-icon"></use>
		                            </svg>
		                        </a>
		                    </li>
		                </ul>
					</div>
				</nav>
				<button class="close-button" id="close-button">Close Menu</button>
				<div class="morph-shape" id="morph-shape" data-morph-open="M0,100h1000V0c0,0-136.938,0-224,0C583,0,610.924,0,498,0C387,0,395,0,249,0C118,0,0,0,0,0V100z">
					<svg width="100%" height="100%" viewBox="0 0 1000 100" preserveAspectRatio="none">
						<path d="M0,100h1000l0,0c0,0-136.938,0-224,0c-193,0-170.235-1.256-278-35C399,34,395,0,249,0C118,0,0,100,0,100L0,100z"/>
						
					</svg>
				</div>
			</div>
		</div>	
	</div>

	

	<div class="container DesktopOnly">
		<div class="row">
			<div class="GridsBlock">
				<div class="col-12 col-md-4">
					<div class="ContactDetails">
						<a href="mailto:heythere@karanverma.co.in">heythere@karanverma.co.in</a><br>
						<a href="tel:+91 011 4606 4192">+91 011 4606 4192</a>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="LogoBox"><a href="/karanverma"><img src="assets/img/logo.png"></a></div>
				</div>
				<div class="col-12 col-md-4">
					<ul class="SocialIcons">                                                    
	                    <li>
	                        <a href="#" target="_blank">
	                            <svg>
	                                <use xlink:href="assets/img/cobold-sprite.svg#youtube-icon"></use>
	                            </svg>
	                        </a>
	                    </li> 
	                    <li>
	                        <a href="#" target="_blank">
	                            <svg>
	                                <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
	                            </svg>
	                        </a>
	                    </li>                           
	                    <li>
	                        <a href="#" target="_blank">
	                            <svg>
	                                <use xlink:href="assets/img/cobold-sprite.svg#facebook-icon"></use>
	                            </svg>
	                        </a>
	                    </li>
	                </ul>
				</div>
			</div>
		</div>
		<div class="MenuContainer">
			<div class="NavContainer">
				<nav>
					<ul>
						<li><a href="profile.php">Profile</a></li>
						<li><a href="">Books</a></li>
						<li><a href="">Speaking Engagements</a></li>
						<li><a href="">Press & Media</a></li>
						<li><a href="">Blogs</a></li>
						<li><a href="">Get in Touch</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</header>
<main>