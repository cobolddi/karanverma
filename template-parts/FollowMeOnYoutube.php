<section class="Section FollowOnYoutube">
	<div class="container">
		<h2>Follow me on Youtube!</h2>
		<h5>CATCH HIM IN THE ACT!</h5>
		<div class="row">
			<div class="col-6 col-md-4">
				<a href="https://www.youtube.com/watch?v=8umZQOusT3g" class="popup-youtube">
					<div class="YoutubeThumbnailBlock">
						<span class="playBtn"><img src="assets/img/play.png"></span>
						<img src="assets/img/youtubethumb.png">
						<h6>Industrialization & Human Ingenuity</h6>
						<p>3 minutes, 45 seconds</p>
					</div>
				</a>
			</div>
			<div class="col-6 col-md-4">
				<a href="https://www.youtube.com/watch?v=8umZQOusT3g" class="popup-youtube">
					<div class="YoutubeThumbnailBlock">
						<span class="playBtn"><img src="assets/img/play.png"></span>
						<img src="assets/img/professor.png">
						<h6>Industrialization & Human Ingenuity</h6>
						<p>3 minutes, 45 seconds</p>
					</div>
				</a>
			</div>
			<div class="col-6 col-md-4">
				<a href="https://www.youtube.com/watch?v=8umZQOusT3g" class="popup-youtube">
					<div class="YoutubeThumbnailBlock">
						<span class="playBtn"><img src="assets/img/play.png"></span>
						<img src="assets/img/chai.png">
						<h6>Industrialization & Human Ingenuity</h6>
						<p>3 minutes, 45 seconds</p>
					</div>
				</a>
			</div>
			<div class="col-6 col-md-4">
				<a href="https://www.youtube.com/watch?v=8umZQOusT3g" class="popup-youtube">
					<div class="YoutubeThumbnailBlock">
						<span class="playBtn"><img src="assets/img/play.png"></span>
						<img src="assets/img/feelingburn.png">
						<h6>Industrialization & Human Ingenuity</h6>
						<p>3 minutes, 45 seconds</p>
					</div>
				</a>
			</div>
			<div class="col-6 col-md-4">
				<a href="https://www.youtube.com/watch?v=8umZQOusT3g" class="popup-youtube">
					<div class="YoutubeThumbnailBlock">
						<span class="playBtn"><img src="assets/img/play.png"></span>
						<img src="assets/img/rockstars.png">
						<h6>Industrialization & Human Ingenuity</h6>
						<p>3 minutes, 45 seconds</p>
					</div>
				</a>
			</div>
			<div class="col-6 col-md-4">
				<a href="https://www.youtube.com/watch?v=8umZQOusT3g" class="popup-youtube">
					<div class="YoutubeThumbnailBlock">
						<span class="playBtn"><img src="assets/img/play.png"></span>
						<img src="assets/img/another.png">
						<h6>Industrialization & Human Ingenuity</h6>
						<p>3 minutes, 45 seconds</p>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>