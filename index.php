<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/LeftImageRightContentWithBorder.php') ?>
<?php @include('template-parts/RightImageLeftContentWithBg.php') ?>
<?php @include('template-parts/CenteredContentWithBg.php') ?>
<?php @include('template-parts/FollowMeOnYoutube.php') ?>
<?php @include('template-parts/SocialLinks.php') ?>
<?php @include('template-parts/FollowMeOnInsta.php') ?>

<?php @include('template-parts/footer.php') ?>
