$(document).ready(function() {
	
	// youtube Videos
	$('.popup-youtube').magnificPopup({        
		type: 'iframe',        
		mainClass: 'mfp-fade',        
		removalDelay: 100,        
		preloader: true,        
		fixedContentPos: false    
	});

	$(".menu-button").click(function(){
		$("body").addClass("show-menu");
	  	$("body").addClass("noScroll");
	});

	
	$(".content-wrap").click(function(){
		$("body").removeClass("show-menu");
		$("body").removeClass("noScroll");
	});

	// external js: masonry.pkgd.js

	$('.grid').masonry({
	  itemSelector: '.grid-item',
	  columnWidth: 160
	});
	console.log("gallery");

})


const hello = (name) => {
  return `hello ${name}`;
};
console.log(hello);